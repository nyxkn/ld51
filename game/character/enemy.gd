extends Entity
class_name Enemy


var kill_order_number := -1

export(AttackInfo.Type) var hurt_by := AttackInfo.Type.PHYSICAL


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

	hurt_by = F.rng.randi_range(0, AttackInfo.Type.size() - 1)

	match hurt_by:
		AttackInfo.Type.PHYSICAL:
			$Sprite.self_modulate = Color.red
		AttackInfo.Type.MAGICAL:
			$Sprite.self_modulate = Color.blue
		AttackInfo.Type.BOTH:
			$Sprite.self_modulate = Color.purple


func set_kill_order(number):
	kill_order_number = number
	$KillOrder.text = str(kill_order_number)


func new_kill_order():
	kill_order_number -= 1
#	$KillOrder.text = str(kill_order_number)


func is_enemy() -> bool:
	return true


func hit_by(entity, attack_info):
	var damage = entity.atk
	if attack_info.type != hurt_by and hurt_by != AttackInfo.Type.BOTH:
		hp -= 0
#		print('wrong attack type')
		G.textbox.show_text('Wrong attack type!')
	else:
		if kill_order_number > 1 and hp - damage <= 0:
			# hit wrong kill order
			hp -= 0
			G.textbox.show_text('Wrong kill order!')

		else:
			hp -= damage

	.hit_by(entity, attack_info)


func ai_turn():
	var player_characters_alive = []
	for p in player_characters:
		if not p.dead:
			player_characters_alive.append(p)
	print(player_characters)

	if player_characters_alive.size() == 0:
		return

	var rnd = F.rng.randi_range(0, player_characters_alive.size() - 1)
	var target_player = player_characters_alive[rnd]
#	target_player.hit_by(self)
	execute(D.Action.ATTACK, target_player)
