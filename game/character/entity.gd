extends Node2D
class_name Entity


#signal turn_ready
signal selected
signal animation_started
signal animation_ended
signal died


#const atb_total = 256

#enum Type { PLAYER, ENEMY }
#var type: int = Type.PLAYER

export(int) var hp := 1
export(int) var mp := 1
#export(int, 1, 255) var speed := 30
#export(Type) var type := Type.ENEMY
export(int, 1, 255) var atk := 1







#var atb_gauge: float = 0.0
# ff6: ~7.5s at inital stats, ~1.5s at max 255
#var atb_increment: float = (speed + 20) * 0.66

#var active := true # are we processing turn gauge (e.g. stop effect)
var dead := false

var player_characters: Array
export(int) var direction := 1

var _game: Node

onready var __highlight = $Highlight


func _ready() -> void:
	update_ui()

	__highlight.hide()
#	__highlight.set_as_toplevel(true)

#	atb_gauge = F.rng.randi_range(0, atb_total)
#	Log.d(["initial time to turn", float(atb_total - atb_gauge) / atb_increment])

#	if type == Type.PLAYER:
#	if is_enemy():
#
#	else:


	$Sprite/Sword.hide()
	$Sprite.scale.x *= direction

	$Sprite.frame = F.rng.randi_range(0, 1)


func is_enemy() -> bool:
	return false

func update_ui():
	$HP.value = hp
#	$MP.value = mp


func _process(delta: float) -> void:
	pass
#	if not _game.turns_paused and not dead:
#		atb_gauge += atb_increment * delta
#		$TurnGauge.value = atb_gauge
#		if atb_gauge >= atb_total:
#			atb_gauge = 0
#			emit_signal("turn_ready")





func _on_Selection_pressed() -> void:
	emit_signal("selected")


func hit_by(entity, attack_info):
#	var damage = entity.atk
#	hp -= damage
	update_ui()
	if hp <= 0:
		dead = true
		emit_signal("died")


func execute(action, target):
	if action == D.Action.ATTACK:
		var initial_pos = global_position
		var new_pos = target.global_position
		var target_texture = target.get_node("Sprite").frames.get_frame("idle", 0)
		var texture = $Sprite.frames.get_frame("idle", 0)
		var offset = target_texture.get_width() * 2 / 2.0 + texture.get_width() * 2 / 2.0 + 16
		new_pos.x += offset * -direction

		emit_signal("animation_started")
		$Tween.interpolate_property(self, "global_position", null, new_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_all_completed")

		$AnimationPlayer.play("attack")
		SoundManager.play("attack")
		G.shake.add_trauma(0.8)
		yield($AnimationPlayer, "animation_finished")
		var ai = AttackInfo.new()
		ai.type = AttackInfo.Type.PHYSICAL
		ai.damage = atk
		target.hit_by(self, ai)


		$Tween.interpolate_property(self, "global_position", null, initial_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_all_completed")
		emit_signal("animation_ended")

	if action == D.Action.MAGIC:
		var initial_pos = global_position
#		var new_pos = target.global_position
		var new_pos = initial_pos
		var target_texture = target.get_node("Sprite").frames.get_frame("idle", 0)
		var texture = $Sprite.frames.get_frame("idle", 0)
		var offset = target_texture.get_width() * 2 / 2.0 + texture.get_width() * 2 / 2.0 + 16
		new_pos.x += (64+32) * direction

		emit_signal("animation_started")
		$Tween.interpolate_property(self, "global_position", null, new_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_all_completed")

		$AnimationPlayer.play("magic")
		target.get_node("Fire").show()
		SoundManager.play("magic")
		G.shake.add_trauma(0.8)
		yield($AnimationPlayer, "animation_finished")
		target.get_node("Fire").hide()
		var ai = AttackInfo.new()
		ai.type = AttackInfo.Type.MAGICAL
		ai.damage = atk
		target.hit_by(self, ai)


		$Tween.interpolate_property(self, "global_position", null, initial_pos, 0.2, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween, "tween_all_completed")
		emit_signal("animation_ended")
