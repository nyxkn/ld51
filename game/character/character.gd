extends Entity
class_name Character


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

#	$Sprite.self_modulate = Color.blue

func hit_by(entity, attack_info):
	var damage = entity.atk
	hp -= damage

	.hit_by(entity, attack_info)
