extends Node


func _enter_tree() -> void:
	pass


func _ready() -> void:
	pass
#	PauseScreen.allowed = true

	Log.d("setting up a new game")

	G.gametime_stopwatch = Stopwatch.new()
	G.add_child(G.gametime_stopwatch)
	G.gametime_stopwatch.start()

	G.stage = 1

#	G.get_node("Battle").volume_db = -80
	G.get_node("Battle").play()

	F.change_scene(Config.scenes.battle, false)


func _exit_tree() -> void:
	pass


func _input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_input", name)


func _gui_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_gui_input", name)


func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

	if event.is_action_pressed("ui_select"):
		pass

	if event is InputEventKey:
		if event.scancode == KEY_1:
			pass


func _unhandled_key_input(event: InputEventKey) -> void:
	pass
#	Utils.log_event(event, "_unhandled_key_input", name)


func _process(delta: float) -> void:
	pass
