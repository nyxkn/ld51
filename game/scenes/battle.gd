extends Control
#class_name Battle

#signal character_selected
signal turn_ended
signal player_character_turn_finished

#var all_entities := []
var focusables := []
var characters := []
var enemies := []


var active_character_id: int = 0
#var	turns_paused := false

var turn_gauge := 10.0
var turn_gauge_paused := false
var turn_gauge_speed := 1

enum Phase { NONE, PLAYER_TURN, ENEMY_TURN, INTERMISSION }
var phase: int = Phase.NONE


var last_focused_control: Control
var focused_control_history := []


enum Focus { NONE, MENU, PLAYER, ENEMY }
var current_focus: int = Focus.NONE

var current_action: int = D.Action.NONE

var action_running: bool = false

var cursor_sound_enabled: bool = false


var stage_node


func _enter_tree() -> void:
	pass


func shuffle_menu():
	var prev_focused = ""
	for c in $"%MenuChoices".get_children():
		var ctrl: Control = c
		if ctrl.has_focus():
			prev_focused = ctrl.name
			break

	var menu_choices = $"%MenuChoices".get_children()
	for e in menu_choices:
		$"%MenuChoices".remove_child(e)
	menu_choices.shuffle()
	for e in menu_choices:
		$"%MenuChoices".add_child(e)

	if prev_focused:
		$"%MenuChoices".get_node(prev_focused).grab_focus()

#	if not focused:
#		$"%MenuChoices".get_child(0).grab_focus()

#	if current_focus != Focus.MENU:
#		set_focus(Focus.MENU)
#	var focused_control = get_focus_owner()
#	if focused_control:
#		update_cursor(focused_control)


func _ready() -> void:
	pass
#	PauseScreen.allowed = true

	Log.i(["stage", G.stage])


	for b in $"%MenuChoices".get_children():
		b.connect("pressed", self, "on_menu_button", [b])

	get_tree().root.connect("gui_focus_changed", self, "on_gui_focus_changed")

	$StageNum.value = G.stage

	G.shake = $Camera2D/Shake

	G.battle = self

	G.textbox = $TextBox

	load_stage()

	var tween = G.get_node("Tween")
	tween.interpolate_property(G.get_node("Battle"), "volume_db", null, 0, 2.0)
	tween.start()

	yield(F.wait(1.5), "completed")


	if G.stage > 9:
		shuffle_menu()
		$TextBox.show_text("Menu shuffle!")
		$ShuffleMenu.wait_time = F.rng.randi_range(5, 15)
		$ShuffleMenu.start()
		$ShuffleKill.wait_time = F.rng.randi_range(5, 10)
		$ShuffleKill.start()
	elif G.stage > 6:
		shuffle_menu()
		$TextBox.show_text("Menu shuffle!")
		$ShuffleKill.wait_time = F.rng.randi_range(3, 8)
		$ShuffleKill.start()
	elif G.stage > 3:
#		$ShuffleKill.wait_time = 20 - G.stage / 2
		$ShuffleKill.wait_time = F.rng.randi_range(1, 5)
		$ShuffleKill.start()


	G.gametime_stopwatch.paused = false

	start_player_turn()


func load_stage() -> void:
#	var stage = load("res://scenes/stage.tscn").instance()
#	add_child(stage)

	var stage = $Stage
	stage_node = stage

	var player_team = stage.get_node("Player")
	var enemy_team = stage.get_node("Enemies")

	for c in player_team.get_children():
		characters.append(c)

	for c in enemy_team.get_children():
		enemies.append(c)
		c.player_characters = characters

	var all_characters = characters + enemies

#	for c in get_tree().get_nodes_in_group("character"):
	for c in all_characters:
		c._game = self
#		c.connect("turn_ready", self, "on_turn_ready", [c])
		c.connect("selected", self, "on_character_selected", [c])
		c.connect("died", self, "on_character_died", [c])


	$Menu.hide()

	for c in all_characters:
		focusables.append(c.get_node("Selection"))
	for c in $"%MenuChoices".get_children():
		focusables.append(c)

	set_focus(Focus.NONE)


func _exit_tree() -> void:
	pass


func _input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_input", name)


func _gui_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_gui_input", name)


func _unhandled_input(event: InputEvent) -> void:
	pass
#	Utils.log_event(event, "_unhandled_input", name)

	if event.is_action_pressed("ui_cancel"):
		if current_focus == Focus.ENEMY:
			set_focus(Focus.MENU)
			for i in range(focused_control_history.size() - 1, -1, -1):
				var c = focused_control_history[i]
				if c.get_parent().name == "MenuChoices":
					c.grab_focus()
					break
			SoundManager.play("cancel")
#		last_focused_control.grab_focus()
#		SoundManager.play("cancel")

#	if event.

#	if event is InputEventKey:
#		if event.scancode == KEY_0:
#			to_end_screen()
#		if event.scancode == KEY_1:
#			turn_gauge = 0.5

#	if phase == Phase.INTERMISSION and event.is_action_pressed("ui_select"):
#		get_tree().reload_current_scene()


func _unhandled_key_input(event: InputEventKey) -> void:
	pass
#	Utils.log_event(event, "_unhandled_key_input", name)


func _process(delta: float) -> void:
	pass
#	Log.i(G.gametime_stopwatch.time)

	var focused_control := get_focus_owner()
	if focused_control and phase == Phase.PLAYER_TURN:
		$Cursor.show()
		update_cursor(focused_control)
	else:
		$Cursor.hide()

	if phase == Phase.PLAYER_TURN and not turn_gauge_paused:
		turn_gauge -= delta * turn_gauge_speed
		$TurnGauge.value = turn_gauge
		if turn_gauge <= 0.0:
	#		emit_signal("turn_ended")
			if action_running:
				Log.d("action running, waiting")
				turn_gauge_paused = true
				yield(self, "player_character_turn_finished")
				if phase != Phase.INTERMISSION:
					turn_gauge_paused = false
					stop_player_turn()
					start_enemy_turn()
			else:
				Log.d("gauge 0")
				stop_player_turn()
				start_enemy_turn()


	$PlayTime.value = "%.1f" % G.gametime_stopwatch.time


func update_cursor(focused_control):
#	Log.d(["updating cursor"])
	$Cursor.rect_position = focused_control.rect_global_position
	$Cursor.rect_position.x += focused_control.rect_size.x + 16
	$Cursor.rect_position.y += focused_control.rect_size.y / 2 - $Cursor.rect_size.y / 2


func on_gui_focus_changed(control: Control):
#	var focused_control := get_focus_owner()
	var focused_control := control
#	if focused_control:
	update_cursor(focused_control)
#	$Cursor.show()
#	else:
#		$Cursor.hide()

#	if focused_control and focused_control != last_focused_control:
	if focused_control.name == "Selection" and last_focused_control.get_parent().name == "MenuChoices":
		pass
#	elif focused_control.name == "NextBattle":
#		pass
	elif cursor_sound_enabled:
		SoundManager.play("cursor")

#	if focused_control != last_focused_control:
	last_focused_control = focused_control
	focused_control_history.append(focused_control)


func start_enemy_turn():
	Log.d("start enemy turn")
	phase = Phase.ENEMY_TURN

	for c in characters:
		c.z_index = 0
	for e in enemies:
		e.z_index = 9

	G.shake.add_trauma(1.0)
	SoundManager.play("enemy_turn2")

	yield(F.wait(0.5), "completed")

	for e in enemies:
		e.ai_turn()
		yield(e, "animation_ended")

	yield(F.wait(1.0), "completed")

	start_player_turn()


func start_player_turn():
	Log.d("start player turn")
	turn_gauge = 10.0
	turn_gauge_speed = 1
	phase = Phase.PLAYER_TURN
	for c in characters:
		c.z_index = 9
	for e in enemies:
		e.z_index = 0


	for i in characters.size():
#		if active_character_id < characters.size() - 1:
		if not characters[i].dead:
#		found_player = true
			start_player_character_turn(i)
			break

#	start_player_character_turn(0)



# add this function to nframework
func to_end_screen():
	G.gametime_stopwatch.stop()

	F.change_scene(Config.scenes.ending)


func next_stage():
	Log.d("next stage")
	G.stage += 1
	G.gametime_stopwatch.paused = true
	G.stage_clear_time.append(G.gametime_stopwatch.time)
	Log.i("stage cleared")
	phase = Phase.INTERMISSION
	$ShuffleKill.stop()
	$ShuffleMenu.stop()

#	var tween = get_tree().create_tween()
#	tween.tween_property($Battle, "volume_db", -80, 1)

#	var tween = Tween.new()
#	add_child(tween)
	var tween: Tween = G.get_node("Tween")
	tween.interpolate_property(G.get_node("Battle"), "volume_db", 0, -80, 2.0, Tween.TRANS_CIRC, Tween.EASE_IN)
#	tween.interpolate_property(
	tween.start()

#	$Battle.volume_db = -80
#	tween.tween_property($Fanfare, "volume_db", 0, 1.0)
	yield(F.wait(1.0), "completed")

	var intermission_screen = load("res://scenes/intermission.tscn").instance()
	add_child(intermission_screen)
#	yield(F.wait(
#	turn_gauge_pausedd = true
#	intermission = true


func start_player_character_turn(character_id):
	if phase != Phase.PLAYER_TURN:
		return

	Log.d(["start player character turn: player", character_id])

#	Log.d(["player", character_id, "turn"])
	active_character_id = character_id
	var character = characters[character_id]
	character.__highlight.show()
	show_menu()


func show_menu():
	$Menu.show()
	set_focus(Focus.MENU)
	$"%MenuChoices".get_child(0).grab_focus()
	update_cursor($"%MenuChoices".get_child(0))
	SoundManager.play("menu_open")

	yield(F.wait(0.1), "completed")
	cursor_sound_enabled = true


func hide_menu():
	set_focus(Focus.NONE)
	$Menu.hide()
	cursor_sound_enabled = false


func finish_player_character_turn():
	Log.d("finish player character turn")

	stop_player_turn()

	action_running = false

	if enemies.size() == 0:
		print('enemies 0')
		next_stage()
		return

	if phase == Phase.PLAYER_TURN:
#		print(enemies.size())
#		var alive_characters = characters
		var found_player = -1
		for i in range(active_character_id + 1, characters.size()):
#		if active_character_id < characters.size() - 1:
			if not characters[i].dead:
				found_player = i
				break

		emit_signal("player_character_turn_finished")

		if found_player == -1:
			turn_gauge_speed = 10
		else:
			start_player_character_turn(found_player)



func stop_player_turn():
	Log.d("stop player turn")

#	if active_character_id < characters.size():
	var character = characters[active_character_id]
#	if active_character: # we weren't obliterated
	character.__highlight.hide()
#	character = null
	hide_menu()


func attack_on_enemy():

	set_focus(Focus.ENEMY)
	enemies[0].get_node("Selection").grab_focus()

#	var target_character = yield(self, "character_selected")[0]
##	var target_character
#
##	if current_focus == Focus.ENEMY:
#	set_focus(Focus.NONE)
#	var origin_character = characters[active_character_id]
#	origin_character.execute(type, target_character)
#	yield(origin_character, "animation_ended")
#	finish_player_character_turn()



func on_menu_button(b: Button) -> void:
	Log.d(["menu button pressed:", b.name])

	current_action = D.Action.keys().find(b.name.to_upper())

	match current_action:
		D.Action.ATTACK:
			attack_on_enemy()
		D.Action.MAGIC:
			attack_on_enemy()
		D.Action.SKIP:
			set_focus(Focus.NONE)
			finish_player_character_turn()

	SoundManager.play("selection")


func set_focus(focus: int):
	for c in focusables:
		c.focus_mode = FOCUS_NONE

	match focus:
		Focus.MENU:
			for c in $"%MenuChoices".get_children():
				c.focus_mode = FOCUS_ALL
		Focus.PLAYER:
			for n in characters:
				n.get_node("Selection").focus_mode = FOCUS_ALL
		Focus.ENEMY:
			for n in enemies:
				n.get_node("Selection").focus_mode = FOCUS_ALL

	current_focus = focus


func on_character_selected(target_character):
	Log.d("character selected")
#	var target_character = yield(self, "character_selected")[0]
#	var target_character
	action_running = true

#	print(current_action)
	if current_action == D.Action.ATTACK or current_action == D.Action.MAGIC:
		set_focus(Focus.NONE)
		var origin_character = characters[active_character_id]
		origin_character.execute(current_action, target_character)
		yield(origin_character, "animation_ended")
		finish_player_character_turn()

	SoundManager.play("selection")
#	emit_signal("character_selected", [character])


func on_character_died(character):
	if character is Enemy:
		enemies.erase(character)
		focusables.erase(character.get_node("Selection"))
		character.queue_free()
		get_tree().call_group("enemy", "new_kill_order")
	else:
		character.get_node("Sprite").self_modulate = Color(0, 0, 0, 0.5)
		character.get_node("Sprite").playing = false
#		characters.erase(character)
#		character.d
		var all_dead = true
		for c in characters:
			if not c.dead:
				all_dead = false

		if all_dead:
			to_end_screen()
#			print("gameover")
#			F.paused = true



func _on_ShuffleKill_timeout() -> void:
	$ShuffleKill.stop()
	$ShuffleKill.wait_time = F.rng.randi_range(5, 10)
	$ShuffleKill.start()
	stage_node.shuffle_kill_order()
	$TextBox.show_text("Kill order shuffle!")


func _on_ShuffleMenu_timeout() -> void:
	$ShuffleMenu.stop()
	$ShuffleMenu.wait_time = F.rng.randi_range(5, 15)
	$ShuffleMenu.start()
	shuffle_menu()
	$TextBox.show_text("Menu shuffle!")
