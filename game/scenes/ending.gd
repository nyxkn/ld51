extends Control


var keypress_enabled: bool = false


func _ready() -> void:
	G.totaltime_stopwatch.paused = true

#	var score = G.stage * 100 + (G.gametime_stopwatch.time / G.stage) * 10
#	score = round(score)

	var score = 0
	var last_clear_time = 0
	for i in G.stage_clear_time.size():
		var t = G.stage_clear_time[i]
		var clear_time = t - last_clear_time
		last_clear_time = t
		score += 100 * (1.0 / clear_time)

	score = round(score)

	if score > G.best_score:
		G.best_score = score

	if G.stage > G.best_stage:
		G.best_stage = G.stage

	$Time.value = "%.1f" % G.gametime_stopwatch.time
	$Stage.value = G.stage
	$Score.value = score
	$BestScore.value = G.best_score

#	$Desc2.text = "This current playthrough lasted " + str(G.gametime_stopwatch.time) + " seconds"
#	$Desc3.text = "You spent " + str(G.totaltime_stopwatch.get_time()) + " seconds playing this game"

	yield(get_tree().create_timer(1), "timeout")
	keypress_enabled = true



func _input(event) -> void:
	if keypress_enabled:
		if (event.is_action_pressed("ui_accept")
			or event.is_action_pressed("ui_select")
			or event.is_action_pressed("ui_cancel")
			or event.is_action_pressed("ui_click")):

			# unless we specify the time it takes as long as it took to get here

			G.stop_all()

			F.change_scene(Config.scenes.main_menu, true, 0.25)
