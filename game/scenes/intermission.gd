extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$"%NextBattle".grab_focus()

	G.play_fanfare()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_NextBattle_pressed() -> void:
	G.fade_out_intermission()

	F.change_scene(Config.scenes.battle)
