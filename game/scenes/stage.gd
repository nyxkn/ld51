extends Node2D

const CharacterScene = preload("res://character/character.tscn")
const EnemyScene = preload("res://character/enemy.tscn")

var kill_order := []



func _ready() -> void:
	var enemy_num = min(1 + round(G.stage * 1 / 3), 12)
	var player_num = min(4 + round(G.stage / 5), 8)

	var player_space = Vector2(240, 180)
	var enemy_space = Vector2(320, 180)

#	var columns = player_num / 4 + 1
	for i in player_num:
		var c = CharacterScene.instance()
		$Player.add_child(c)
		var column = i / 4
		c.position = Vector2(160 + column * 64, 80 + 64 * (i % 4))
#		if column % 2 == 1:
#			c.position.y += 32
#		print(c.position)


	for i in enemy_num:
		var e = EnemyScene.instance()
		e.direction = -1
		$Enemies.add_child(e)
		var column = i / 4
		e.position = Vector2(400 + column * 64, 80 + 64 * (i % 4))
#		if column % 2 == 1:
#			e.position.y += 32

	shuffle_kill_order()


func shuffle_kill_order():
	kill_order = range($Enemies.get_child_count())
	kill_order.shuffle()

	for i in $Enemies.get_child_count():
		$Enemies.get_child(i).set_kill_order(kill_order[i] + 1)


