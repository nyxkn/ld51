extends Node

signal finished

var finished := false
var thread := Thread.new()

func _init() -> void:
	thread.start(self, "thread_function")

# we're running this as a thread because it locks for a few seconds when network is down
func thread_function(userdata):
	# rename _ready to init in the SilentWolf autoload
	# this is so we can manually call it when we want rather than letting it run automatically
	# you have to do this again if you update SilentWolf

	# for some reason assert won't print the error message
	# but still stop the code (of the thread only)
#		assert(SilentWolf.has_method("init"), "error initializing silentwolf: ensure you have renamed _ready to init in SilentWolf.gd")

	# grabbing the silentwolf singleton through the scenetree so that it doesn't throw error if singleton isn't loaded
	var SilentWolf = get_tree().root.get_node("SilentWolf")

	# makeshift assert
	if ! SilentWolf.has_method("init"):
		Log.e("error initializing silentwolf: ensure you have renamed _ready to init in SilentWolf.gd")
		return # warning: this doesn't queue_free the object at the end

	SilentWolf.init()

	var silentwolf_json = FileUtils.read_json("res://data/silentwolf.json")
	SilentWolf.configure(silentwolf_json)

	SilentWolf.configure_scores({
		"open_scene_on_close": Config.scenes.main_menu
	})

	finished = true
	emit_signal("finished")
	queue_free()

func _exit_tree() -> void:
	pass
	# this line also locks the application from quitting until thread completes
	# this is probably not what you want
	# especially since silentwolf waits for a few seconds for a connection
	# and that doesn't let us quit while it's happening
	thread.wait_to_finish()
	# but debugger complains if we don't add this line. what to do?

