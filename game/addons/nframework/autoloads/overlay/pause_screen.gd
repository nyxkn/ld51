extends CanvasLayer

## this is the pause system
## all of the ui can be replaced with anything you want
## it doesn't necessarily have to be a full menu
## e.g. a basic alpha colorrect with a "paused" title in the middle
## eventually you might consider splitting the ui part of this in a different scene
## that will make more sense if you want to support different pause screen ui layouts

# public

# each scene is responsible for allowing the pause menu
# it is automatically disabled on scene change
var allowed: bool = false setget set_allowed

# private
var settings_opened: bool = false
var settings_menu: Control = null

# this needs to be onready otherwise it will get called before Config has time to initialize scenes
onready var SettingsMenu = load(Config.scenes.settings_menu)

onready var button_resume: Button = find_node("Resume")
onready var screen: Control = $Screen


func _ready():
	screen.visible = false
	screen.theme = load(Config.theme)
	Utils.setup_focus_grabs_on_mouse_entered(screen)
	F.connect("scene_changed", self, "scene_changed")

	if Config.disable_settings:
#		$"Screen/PauseMenu/PanelContainer/VBoxContainer-MinSize/Settings".visible = false
		$"%Settings".visible = false


func _input(event) -> void:
	# _input is still called despite the scenetree being paused
	# because pause_mode of this node is set to process

	# this defines pause button behaviour
	# in this case we're pausing and also showing the menu
	if event.is_action_pressed("ui_escape"):
		get_tree().set_input_as_handled()
		if screen.visible and !settings_opened:
			F.paused = false
			show_screen(false)
		else:
			# need to use self to trigger the setget function
			F.paused = true
			show_screen(true)


func scene_changed() -> void:
	# disabling the menu if the scene has been changed
	# every scene is responsible for setting whether a pausemenu is allowed
	self.allowed = false


func set_allowed(value: bool) -> void:
	allowed = value
	set_process_input(allowed)


func show_screen(show: bool) -> void:
	screen.visible = show
	if show:
		button_resume.grab_focus()


func _on_Resume_pressed():
	Log.d("resume pressed", name)
	show_screen(false)
	F.paused = false


func _on_MainMenu_pressed() -> void:
	F.change_scene(Config.scenes.main_menu)

	# TransitionOverlay has to be set to process during pause for this to work
	# otherwise just do the change_scene without transitions
	yield(F, "scene_faded_out")
	show_screen(false)


func _on_Settings_pressed() -> void:
	# we have to hide this so it doesn't conflict with gui navigation in settings
	screen.get_node("PauseMenu").hide()

	settings_opened = true
	settings_menu = SettingsMenu.instance()
	settings_menu.connect("menu_closed", self, "on_SettingsMenu_closed")
	screen.add_child(settings_menu)


func on_SettingsMenu_closed() -> void:
#	remove_child(settings_menu)
#	settings_menu.queue_free()
	screen.get_node("PauseMenu").show()
	button_resume.grab_focus()
	settings_opened = false
