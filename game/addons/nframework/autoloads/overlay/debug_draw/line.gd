tool
extends Node2D
class_name Line


export(Vector2) var from := Vector2.ZERO
export(Vector2) var to := Vector2.ZERO

export(Color) var color := Color.white setget set_color
export(int, 1, 10) var width := 4 setget set_width


func _draw() -> void:
	draw_line(from, to, color, width)


func init(from: Vector2, to: Vector2):
	self.from = from
	self.to = to
	update()
	return self


func set_from(value):
	from = value
	update()

func set_to(value):
	to = value
	update()

func set_color(value):
	color = value
	update()

func set_width(value):
	width = value
	update()
