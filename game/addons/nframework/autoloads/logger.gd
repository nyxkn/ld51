tool
extends Node


enum Level {
	# debug - always meant to be disabled in release
	# VERBOSE, # from godot-logger
	# TRACE, # log4j
	DEBUG, # prints used for debugging behaviour or tracking things

	# informational - these stay in release. they provide meaningful information to the user
	INFO, # inform the user/yourself of something
	WARN, # provide a warning
	ERROR, # notify of an error
	# FATAL, # log4j
}

const output_format = "[{TIME}] ({TICKS}) [{LVL}] [{CAT}] {MSG}"

# format of the {TIME} block
# example with all supported placeholders: "YYYY.MM.DD hh.mm.ss"
const time_format = "hh:mm:ss.ms"


var _registered_context: String = ""


## objects can be an array or a single basic type
func print_log(objects, context, level: int = Level.DEBUG, objects_only: bool = false):
	# Don't log if globally off
#	var tree = get_tree()
#	if not tree:
#		print("no tree when trying to print: " + str(objects))
#
#	if tree:
#		var config = get_tree().root.get_node("Config")
#	#	if !Config.log_show: return
#		if not config.log_show:
#			return
#
#		# Don't log if this Level selectively hidden
#		if config.log_hide_level.has(level) and config.log_hide_level[level] == true: return

	# Format objects depending on type
	var objects_str = ""
	if objects is Array:
		for obj in objects:
			objects_str += str(obj) + " "
		objects_str = objects_str.trim_suffix(" ")
	else:
		objects_str = str(objects)

	var context_str = ""
	if context:
		if context is String:
			context_str = context
		elif context is Object:
			context_str = context.get_script().resource_path.get_file().trim_suffix(".gd")
	else:
		context_str = _registered_context

	var final_str: String
	if objects_only:
		final_str = objects_str
	else:
		final_str = _format(level, context_str, objects_str)

	if level == Level.ERROR:
		printerr(final_str)
		push_error(final_str)
	elif level == Level.WARN:
		print(final_str)
		push_warning(final_str)
	else:
		print(final_str)


func d(objects, context = ""):
	self.print_log(objects, context, Level.DEBUG)


func i(objects, context = ""):
	self.print_log(objects, context, Level.INFO)


func w(objects, context = ""):
	self.print_log(objects, context, Level.WARN)


func e(objects, context = "", error_code = 0):
	if error_code > 0:
		var error_str = ERROR_MESSAGES[error_code].rstrip(".")
#		var error_message = str("(", error_code, " - ", error_str, ")")
#		var error_message = str("[ ", error_str, " (", error_code, ") ]")
		var error_message = str("--- ", error_str, " (", error_code, ")")
		objects.append(error_message)

	self.print_log(objects, context, Level.ERROR)


# generic print. can be used instead of print()
func p(objects):
	self.print_log(objects, "print", Level.DEBUG)

# print no time
func pnt(objects):
	self.print_log(objects, "print", Level.DEBUG, true)


# inspired by godot heightmap plugin
# https://github.com/Zylann/godot_heightmap_plugin/blob/master/addons/zylann.hterrain/util/logger.gd
# but instead of returning a new instance from a static function
# we simply initialize when already instanced
# var Log = preload("res://addons/nsound/logger.gd").new().init(self)
# this has the advantage that it's fully compatible with using this class as an autoload
# it's up to the user whether to initialize the instance or just call the autoload methods
func init(owner):
	if owner is Object:
		_registered_context = owner.get_script().resource_path.get_file().trim_suffix(".gd")
	elif owner is String:
		_registered_context = owner

	return self


# Some of this code is taken from godot-logger
# https://github.com/KOBUGE-Games/godot-logger/blob/master/logger.gd

func _format(level, context, message):
	var output = output_format

	output = output.replace("{LVL}", Level.keys()[level])
	output = output.replace("{CAT}", context)
	output = output.replace("{MSG}", message)
	output = output.replace("{TIME}", _get_formatted_datetime(time_format))
	output = output.replace("{TICKS}", _get_formatted_ticks())

	output = output.replace(" [] ", " ")

	return output


# Format the fields:
# * YYYY = Year
# * MM = Month
# * DD = Day
# * hh = Hour
# * mm = Minutes
# * ss = Seconds
# * ms = Milliseconds
# e.g. time_string can look like "hh:mm:ss.ms"
func _get_formatted_datetime(time_string):
#	var datetime = OS.get_datetime()
	var msec_epoch := OS.get_system_time_msecs()
	var datetime = Time.get_datetime_dict_from_unix_time(int(msec_epoch * 0.001))
	var msec = msec_epoch % 1000

	var result = time_string
	result = result.replace("YYYY", "%04d" % [datetime.year])
	result = result.replace("MM", "%02d" % [datetime.month])
	result = result.replace("DD", "%02d" % [datetime.day])
	result = result.replace("hh", "%02d" % [datetime.hour])
	result = result.replace("mm", "%02d" % [datetime.minute])
	result = result.replace("ss", "%02d" % [datetime.second])
	result = result.replace("ms", "%03d" % msec)

	return result


func _get_formatted_ticks():
	var ticks = OS.get_ticks_usec()
	var seconds = ticks * 0.000001
	return TimeUtils.format_seconds(seconds, TimeUtils.SecondsResolution.US)


# Maps Error code to strings.
# This might eventually be supported out of the box in Godot,
# so we'll be able to drop this.
const ERROR_MESSAGES = {
	OK: "OK.",
	FAILED: "Generic error.",
	ERR_UNAVAILABLE: "Unavailable error.",
	ERR_UNCONFIGURED: "Unconfigured error.",
	ERR_UNAUTHORIZED: "Unauthorized error.",
	ERR_PARAMETER_RANGE_ERROR: "Parameter range error.",
	ERR_OUT_OF_MEMORY: "Out of memory (OOM) error.",
	ERR_FILE_NOT_FOUND: "File: Not found error.",
	ERR_FILE_BAD_DRIVE: "File: Bad drive error.",
	ERR_FILE_BAD_PATH: "File: Bad path error.",
	ERR_FILE_NO_PERMISSION: "File: No permission error.",
	ERR_FILE_ALREADY_IN_USE: "File: Already in use error.",
	ERR_FILE_CANT_OPEN: "File: Can't open error.",
	ERR_FILE_CANT_WRITE: "File: Can't write error.",
	ERR_FILE_CANT_READ: "File: Can't read error.",
	ERR_FILE_UNRECOGNIZED: "File: Unrecognized error.",
	ERR_FILE_CORRUPT: "File: Corrupt error.",
	ERR_FILE_MISSING_DEPENDENCIES: "File: Missing dependencies error.",
	ERR_FILE_EOF: "File: End of file (EOF) error.",
	ERR_CANT_OPEN: "Can't open error.",
	ERR_CANT_CREATE: "Can't create error.",
	ERR_QUERY_FAILED: "Query failed error.",
	ERR_ALREADY_IN_USE: "Already in use error.",
	ERR_LOCKED: "Locked error.",
	ERR_TIMEOUT: "Timeout error.",
	ERR_CANT_CONNECT: "Can't connect error.",
	ERR_CANT_RESOLVE: "Can't resolve error.",
	ERR_CONNECTION_ERROR: "Connection error.",
	ERR_CANT_ACQUIRE_RESOURCE: "Can't acquire resource error.",
	ERR_CANT_FORK: "Can't fork process error.",
	ERR_INVALID_DATA: "Invalid data error.",
	ERR_INVALID_PARAMETER: "Invalid parameter error.",
	ERR_ALREADY_EXISTS: "Already exists error.",
	ERR_DOES_NOT_EXIST: "Does not exist error.",
	ERR_DATABASE_CANT_READ: "Database: Read error.",
	ERR_DATABASE_CANT_WRITE: "Database: Write error.",
	ERR_COMPILATION_FAILED: "Compilation failed error.",
	ERR_METHOD_NOT_FOUND: "Method not found error.",
	ERR_LINK_FAILED: "Linking failed error.",
	ERR_SCRIPT_FAILED: "Script failed error.",
	ERR_CYCLIC_LINK: "Cycling link (import cycle) error.",
	ERR_INVALID_DECLARATION: "Invalid declaration error.",
	ERR_DUPLICATE_SYMBOL: "Duplicate symbol error.",
	ERR_PARSE_ERROR: "Parse error.",
	ERR_BUSY: "Busy error.",
	ERR_SKIP: "Skip error.",
	ERR_HELP: "Help error.",
	ERR_BUG: "Bug error.",
	ERR_PRINTER_ON_FIRE: "Printer on fire error.",
}
