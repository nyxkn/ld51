tool
extends Node

# everything that you want saved/restored needs to be an export(type)

################################################################
# CONST

const CFG_PASS := "config_pass"


################################################################
# LOGGING

# Global switch for showing logs
export(bool) var log_show := true

# All LogCategories are shown by default. Add true to this Dictionary to
# prevent showing  Logs of this Log Category
# removed for now
# var HIDE_LOG_CATEGORY = {}

# All LogLevels are shown by default. Add true to this Dictionary to
# prevent showing Logs of this Log Level
export(Dictionary) var log_hide_level := {}


################################################################
# RESOURCES and PATHS


export(Dictionary) var scenes := {}

var default_scenes := {
	intro = "res://addons/nframework/scenes/intro.tscn",
	ending = "res://addons/nframework/scenes/ending.tscn",
	loading = "res://addons/nframework/scenes/loading.tscn",
	main_menu = "res://addons/nframework/scenes/main_menu.tscn",
	settings_menu = "res://addons/nframework/scenes/settings_menu.tscn",

	# this should point to your game actual entry point
	new_game = "res://game.tscn"
	}


export(String) var levels_path := "res://levels/"
export(String) var data_path := "res://data/"
export(String) var scores_path := "user://"
export(String) var cfg_path := "user://"

################################################################
# OTHER

export(bool) var low_res: bool = true

# enable or disable menu keyboard controls
export(bool) var menu_keyboard_enabled: bool = true

#var PAUSE_ENABLED: bool = true

## Ludum Dare mode
## - disable loading of pre-made assets
## starting with framework/library type of base-code is allowed
## what about menus? a "logo/intro screen" is allowed, but maybe menus are not?
export(bool) var ludum_dare: bool = true

# enable silentwolf
export(bool) var silentwolf: bool = false

# jump straight into the game, skipping the menu or intro screens
export(bool) var straight_to_game := false

export(bool) var disable_settings := false

################################################################
# RUNTIME

# set if we're running as an html application
var html5: bool = false

# holds the selected theme
var theme: String

# holds the editor interface. set from plugin.gd
var editor_interface

var debug: bool = OS.is_debug_build()

var audio_buses: Dictionary


################################################################
# LOGIC

var save_path = cfg_path + "config.cfg"


# _init instead of _ready so this happens as soon as possible
# no need to wait for _ready anyway
func _init() -> void:
	# loading and storing cfg fails on itch.io html
#	load_cfg()

	if OS.get_name() == "HTML5":
		html5 = true

	if not scenes:
		scenes = default_scenes.duplicate()

	if low_res:
		theme = "res://addons/nframework/assets/themes/theme_lowres_tex.tres"
	else:
		# ideally theme_hires would just be an override on top of lowres, without redefining everything
		# should be possible in godot 4
		theme = "res://addons/nframework/assets/themes/theme_hires_tex.tres"

	# we're always returned the same instance so it doesn't matter which editorplugin we use
	# it's easiest to just pull the interface from a new plugin
	# actually we can't make an instance of EditorPlugin from here. not allowed
#	var plugin: EditorPlugin = EditorPlugin.new()
#	editor_interface = plugin.get_editor_interface()
#	plugin.queue_free()



func _exit_tree() -> void:
	pass
#	save_cfg()


func save_cfg() -> void:
	var config = ConfigFile.new()

	var props = get_property_list()
	for prop in props:
		if prop.usage == (PROPERTY_USAGE_SCRIPT_VARIABLE | PROPERTY_USAGE_DEFAULT):
			config.set_value("config", prop.name, get(prop.name))

#	config.save_encrypted_pass(save_path, OS.get_unique_id())
	config.save_encrypted_pass(save_path, CFG_PASS)

	if debug:
		config.save(save_path + "_unencrypted")

#	Log.d("config saved", name)


func load_cfg() -> void:
	if not FileUtils.file_exists(save_path):
		return
#	var cfg = FileUtils.read_cfg(save_path, OS.get_unique_id())
	var cfg = FileUtils.read_cfg(save_path, CFG_PASS)

	if cfg:
		var props = cfg["config"]
		for key in props:
			set(key, props[key])
