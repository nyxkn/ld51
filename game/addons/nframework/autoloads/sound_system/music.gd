extends Node

# https://github.com/nathanhoad/godot_sound_manager/tree/main/addons/sound_manager
# https://github.com/nathanhoad/godot_sound_manager/blob/main/addons/sound_manager/music.gd

const AudioPlayer := preload("audio_player.gd")

# only one music track will be playing
# but we have two players to crossfade between tracks
onready var music_player_pool: AudioPlayer = AudioPlayer.new("Music", 2)

onready var current_player: AudioStreamPlayer = music_player_pool.available_players[0]


func play(resource, crossfade_duration: float = 0.0) -> void:
	# optionally check if track is already playing

	# the fade out should be faster than the fade in
	stop(crossfade_duration * 2)

	## should we move the preparing of the player to a separate function
	## so that you can first prepare, then trigger the fade, then do play?
	var player: AudioStreamPlayer
	if resource is AudioStream:
		print('playing music')
		player = music_player_pool.play(resource)
	else:
		# load from file?
		Log.e("currently only supporting playing a streamresource", name)
		return

	if crossfade_duration > 0:
		fade_volume(player, Music.MIN_DB, 0, crossfade_duration)


func stop(fade_out_duration: float = 0.0) -> void:
	if fade_out_duration <= 0:
		current_player.stop()
	else:
		fade_volume(current_player, 0, Music.MIN_DB, fade_out_duration)


## use -60 for fade out to silence
func fade_volume(stream_player: AudioStreamPlayer, start_volume: float, end_volume: float, duration: float):
	var tween = Tween.new()
	add_child(tween)

	if stream_player.volume_db < end_volume:
		# fade in
		tween.interpolate_property(stream_player, "volume_db", start_volume, end_volume, duration,
			Tween.TRANS_QUAD, Tween.EASE_OUT)
	else:
		# fade out
		# circ is good so that it's a smooth exit but the ending goes down quickly
		tween.interpolate_property(stream_player, "volume_db", start_volume, end_volume, duration,
			Tween.TRANS_CIRC, Tween.EASE_IN)

	tween.start()
	yield(tween, "tween_all_completed")
	tween.queue_free()

	if end_volume <= Music.MIN_DB:
		Log.d(["fade volume reached min_db, stopping playback"], name)
		stream_player.stop()
