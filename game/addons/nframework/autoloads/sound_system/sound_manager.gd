extends Node


const AudioPlayer := preload("audio_player.gd")

var samples := {}

onready var sound_player: AudioPlayer = AudioPlayer.new("SFX")
onready var ui_sound_player: AudioPlayer = AudioPlayer.new("UI")


func _ready() -> void:
	add_child(sound_player)
	add_child(ui_sound_player)

#	var wav_files := FileUtils.get_files_in_dir("res://assets/jfxr", ".wav")
#	load_samples(wav_files)

# resource can be either the name of the loaded sample, or an audiostream
# see _play_sample below
func play(resource, audio_params: AudioParams = AudioParams.new()) -> void:
	_play_sample(resource, sound_player, audio_params)


func play_ui(resource) -> void:
	_play_sample(resource, ui_sound_player)


func _play_sample(resource, audio_player: AudioPlayer, audio_params: AudioParams = AudioParams.new()) -> void:
	if resource is AudioStream:
		audio_player.play(resource, audio_params)
	elif resource is String:
		if samples.has(resource):
			audio_player.play(samples[resource], audio_params)
		else:
			Log.e(["sample not loaded:", resource], name)
			# load on the spot? maybe bad idea


# add samples to the collection. we do the loading
func load_samples(resource_paths: Array) -> void:
	for r in resource_paths:
		# this gets the filename without extension
		r = r.trim_suffix(".import")
		var id = r.get_file().get_basename()

		if samples.has(id):
			Log.e(["Sample with id", id, "already exists"], name)

		# TODO we might want to do this asynchronously in a thread
		# gametemplate has an example
#		var s: String = r
#		s = s.trim_suffix(".import")
		samples[id] = load(r)
		Log.d(["Loaded sample:", id])


func load_samples_folder(folder_path: String) -> void:
	Log.d(["load_samples_folder"])
	var files = FileUtils.get_files_in_dir(folder_path, ".wav.import")
	Log.d(files)
	load_samples(files)


# add samples to the collection. you handle the loading
# pass in an array of Resource objects
func add_samples(resources: Array) -> void:
	for r in resources:
		r = r as Resource
		var id = r.get_path().get_file().get_basename()
		samples[id] = r


# free samples from memory
# pass in an array of samples ids
func remove_samples(ids: Array) -> void:
	for id in ids:
		samples[id].queue_free()
		samples.erase(id)
