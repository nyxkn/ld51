extends Resource
class_name AudioParams

# change of volume in db
var gain := 0.0

# maximum deviation of pitch in octaves. applied both ways. i.e. 1.0 is an octave up and down
# so probably keep this [0-1]
# to use semitones, set this to n/12 where n is the number of semitones of deviation
# this technically affects sample rate, so it also changes time
# 1.0 would make the higher octave sound half as long, while the lower octave becomes twice as long
# keep this in mind, so maybe avoid large values
var pitch_randomization := 0.0

# maximum deviation of volume in db. this we apply only downward
# because if we assume samples to be normalized to 0db, upping the gain will create distortion
# so 10 would make us randomize between 0db and -10db
# db notes:
# +3db means double the power (energy). but +10db is what's perceived as twice as loud
# a 10db change is a factor of 2 change in perceived loudness (+10db is double as loud)
# a 6db change is a factor of 2 change in voltage (+6db is double the voltage)
# a 3db change is a factor of 2 change in power ratio (+3db is double the power)
var volume_randomization := 0.0
