extends Node

onready var settings_nodes = {
	"video": SettingsVideo,
	"audio": SettingsAudio,
	"controls": SettingsControls
	}

onready var save_path = Config.cfg_path + "settings.cfg"

## we want to call these things in here to guarantee order. don't use _ready in the individual scripts
func _ready() -> void:
	load_cfg()

	SettingsVideo.init()
	SettingsControls.init()
	SettingsAudio.init()


#func _exit_tree() -> void:
#	save_cfg()


func save_cfg() -> void:
	var config = ConfigFile.new()

	for settings_node_name in settings_nodes:
		var node = settings_nodes[settings_node_name]

		var props = node.get_property_list()
		for prop in props:
			if prop.usage == (PROPERTY_USAGE_SCRIPT_VARIABLE | PROPERTY_USAGE_DEFAULT):
				# this selects only properties marked as "export" AND with an explicit type hint
				# i.e. "export(bool) var asdf" will be included, but not something like "export var asdf"
				config.set_value(settings_node_name, prop.name, node.get(prop.name))

		# also export custom properties if any are defined
		if node.has_method("export_custom_properties"):
			var custom_props = node.export_custom_properties()
			for category in custom_props:
				for key in custom_props[category]:
					config.set_value(settings_node_name + "_" + category, key, custom_props[category][key])

	config.save(save_path)

	Log.d("settings saved", name)


func load_cfg() -> void:
	if not FileUtils.file_exists(save_path):
		return
	var cfg := FileUtils.read_cfg(save_path)

	for section in cfg:
		var section_split = section.split("_")
		var settings_node_name = section_split[0]

		var category = ""
		if section_split.size() > 1:
			category = section_split[1]

		var node = settings_nodes[settings_node_name]

		var props = cfg[section]
		for key in props:
			node.set(key, props[key])

		if category and node.has_method("restore_custom_properties"):
			var restore_props = { category: cfg[section] }
			node.restore_custom_properties(restore_props)
