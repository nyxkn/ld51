extends Node

## Framework is pretty much just a collection of useful game functions or helpers

# export wouldn't work from here. it should be a scene
#export (String, FILE, "*.tscn") var main_menu: String = "res://addons/nframework/screens/MainMenu.tscn"
# or this should point to defaults, but a config file load could override this
# or poor man's version we just override this manually in game code

# you can possibly add changing scene through signals, that is:
#   emit_signal("change_scene", main_menu)
# this makes more sense if you're trying to decouple things,
# but since this is an autoload it's pointless really. just call directly


signal scene_faded_out
signal scene_changed
signal scene_faded_in
signal scene_ready

signal game_paused(paused)

var paused: bool = false setget set_paused

## instance of rng everyone can use
## make sure you never call randomize on it from elsewhere
## we could enforce that by providing a wrapper to rng class
var rng: RandomNumberGenerator = RandomNumberGenerator.new()

## elapsed time in seconds since engine startup. microseconds resolution
## this will be affected by timescale changes and pause status
## if you need real time you want OS.get_ticks
## keep in mind that this won't get updated within the same function call
## e.g. if you try to time a difference: F.time, do_something, F.time
## F.time will be the same in both cases because we didn't get to run _process
var _time: float = 0.0 setget ,time

var layers := []

var screen_size := Vector2(
	int(ProjectSettings.get_setting("display/window/size/width")),
	int(ProjectSettings.get_setting("display/window/size/height"))
	)

# get_viewport() in here actually returns the root viewport
# still feels more semantically correct to use this
onready var viewport_size := get_viewport().get_visible_rect().size
onready var aspect_ratio: float = viewport_size.x / viewport_size.y


func _ready() -> void:
	# this is set to stop by default already
	# stop is necessary to make the time counter work properly
	# that is if you want it to stop during pause
	pause_mode = Node.PAUSE_MODE_STOP

	_setup_rng()

	# read the layer names into self.layers
	for i in range(1, 33):
		layers.append(ProjectSettings.get_setting("layer_names/2d_physics/layer_" + str(i)))


func _process(delta: float) -> void:
	_time += delta


func time():
	return _time


## returns ticks in seconds
func ticks():
	# get_ticks_msec is simply usec / 1000
	return OS.get_ticks_usec() * 0.000001


func set_paused(value: bool) -> void:
	paused = value
	Log.d(["pause:", paused], name)
	# this also pauses all input
	get_tree().paused = paused
	# things can still receive inputs and act upon them when tree is paused
	# pause only stops all physics and stops calling all process and input functions
	# unless pause_mode is set to process like in this node
	emit_signal("game_paused", paused)


# shortcut for waiting
# use this like so:
# yield(F.wait(1.0), "completed")
func wait(time: float = 0.0) -> void:
	yield(get_tree().create_timer(time), "timeout")


func change_scene(scene, with_transition: bool = true,
		transition_duration: float = 0.0, black_duration: float = 0.1) -> bool:
	if !ResourceLoader.exists(scene):
		Log.e("attempting to load inexistent scene: " + scene, name)
		return false

	var root_viewport: Viewport = get_tree().root

	root_viewport.gui_disable_input = true

	if with_transition:
		TransitionOverlay.fade_out(transition_duration)
		yield(TransitionOverlay, "fade_out_completed")
		emit_signal("scene_faded_out")

	get_tree().change_scene(scene)
	emit_signal("scene_changed")
	Log.d(["scene changed to:", scene], name)

	yield(wait(black_duration), "completed")

	if with_transition:
		TransitionOverlay.fade_in(transition_duration)
		yield(TransitionOverlay, "fade_in_completed")
		emit_signal("scene_faded_in")

	root_viewport.gui_disable_input = false
	if paused: self.paused = false

	# we maybe don't need this signal here, but why not
	emit_signal("scene_ready")
#	Log.d(["scene ready:", scene], name)

	return true


# this returns the bitmask value of the layer
# layers are specified as a bitmask:
# layer 1 is bit 0 and has value 1 (2^0)
# layer 2 is bit 1 and has value 2 (2^1)
# layer 3 is bit 2 and has value 4 (2^2)
# etc.
# https://docs.godotengine.org/en/latest/tutorials/physics/physics_introduction.html#code-example
func get_layer(name) -> int:
	var idx = layers.find(name)

	# this code is to return the layer number. actually not very useful.
#	if idx == -1:
#		return idx
#	else:
#		# layers are numbered 1-32. no 0
#		return idx+1

	return int(pow(2, idx))


# randomizing the seed by default
# but you can also manually provide a seed number for reproducibility
func _setup_rng() -> void:
	# setting up global seed
	# we randomize first, then we get a random int that we then set as the seed
	# this way we know what the seed is and can reuse it later
	randomize()
	var random_seed = randi()
	seed(random_seed)
	Log.i(["Random seed:", random_seed])

	# setting up our own rng, optionally with a custom seed
	rng.randomize()
#	rng.seed = 1234


func p(array: Array) -> void:
	var msg = ""

	for e in array:
		msg += str(e) + " "
	msg.trim_suffix(" ")

	print(msg)

