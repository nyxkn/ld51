extends Node

## the basic way of using a camera is to attach it as a child of what you want to track
## but that isn't very flexible and if you want custom smoothing you might be
## better off manually adjusting the camera position to track what you want to track
## this second approach is what we do here

## adjust shake offset max and shake intensity to change the amount of movement and the speed of movement


enum ShakeType {
	ROTATIONAL = 1
	TRANSLATIONAL = 2
}

export(int, FLAGS, "Rotational", "Translational") var shake_type: int = ShakeType.ROTATIONAL | ShakeType.TRANSLATIONAL
export(NodePath) var tracking: NodePath = ""
#export(OpenSimplexNoise) var noise: OpenSimplexNoise = OpenSimplexNoise.new()
# use this line instead if you want to use a noise texture you can visualize for fine tuning
# TODO eventually remove this and the sprite node
onready var noise: OpenSimplexNoise = $NoiseVisualizer.texture.noise

## speed of trauma decay in trauma units per second
export(float, 0.0, 2.0) var trauma_decay: float = 0.8
## trauma exponent. use [2, 3]. the higher this is, the slower the curve starts
export(int, 2, 3) var trauma_exp: int = 2
## in degrees. at small angle values, it's easier to think in degrees than radians
export(int, 0, 10) var shake_angle_max: int = 1
## pixels. a very aggressive shake is about 10% of screen size. 2-4% is a sane range. 1% is very mild
export(Vector2) var shake_offset_max: Vector2 = Vector2(20, 10)
## multiplier for rate of change of perlin noise. 1.0 seems nice. <0.5 is very slow. 2.0 is rapid
export(float, 0.1, 5.0) var shake_intensity: float = 1.0


var camera: Camera2D

## our internal trauma [0,1]
var trauma: float = 0.0 setget set_trauma
## elapsed time in seconds
var _time: float = 0.0


func _ready() -> void:
	var parent = get_parent()
	assert(parent is Camera2D, "attempting to use camera module " + name + " without a parent camera")
	camera = parent

	noise.seed = F.rng.randi()
	# smallest possible period seems to work well with a time input in seconds
	noise.period = 0.1


func _process(delta: float) -> void:
	_time += delta

	# track
	if tracking:
		var tracked_node := get_node(tracking) as Node2D
		camera.position = tracked_node.position

	if trauma > 0.0:
		# decrease trauma linearly over time
		self.trauma -= trauma_decay * delta
	#	self.trauma = clamp(trauma, 0.0, 1.0)

		var shake_amount = pow(trauma, trauma_exp)

		if shake_type & ShakeType.ROTATIONAL:
			var angle = deg2rad(shake_angle_max) * shake_amount * get_noise(0)
			camera.rotation = angle
		if shake_type & ShakeType.TRANSLATIONAL:
			var offset_x = shake_offset_max.x * shake_amount * get_noise(1)
			var offset_y = shake_offset_max.y * shake_amount * get_noise(2)
			# should we lerp so that the initial displacement is not sudden?
			# but lerp is going to reduce the overall range. probably not
			camera.offset = Vector2(offset_x, offset_y)


func get_noise(seed_offset: int) -> float:
	# plain random generation
#	return F.rng.randf_range(-1.0, 1.0)

	# or perlin noise
	# here we are using the y coordinate as a seed value
	# the alternative would be to change the seed of the noise generator before every call
	# but that seems a bit more clumsy
	# y has to be large enough so that the distance guarantees you're looking at unrelated data
	# 1000 seems like a safe value, but less would probably also work
	var random = noise.get_noise_2d(_time * shake_intensity, seed_offset * 1000)
	return random


func add_trauma(amount: float) -> void:
	self.trauma += amount
#	trauma = clamp(trauma, 0.0, 1.0)


func set_trauma(value: float) -> void:
	trauma = clamp(value, 0.0, 1.0)

