tool
extends MarginContainer


# to use, unpack as editable children, then add your content under the Content node


onready var content := $VBoxContainer/Content


func _ready() -> void:
	if content.get_child_count() == 0:
		pass
		# TODO: fix this. we're skipping the print because it happens way early when the scenetree hasn't been fully setup
		# which triggers an error in logger.gd print function when we try to read the log hide values from Config
#		Log.w("you need to add your own content under the Content node", name)

	get_node("%ShowFold").connect("pressed", self, "_on_ShowFold_pressed")
	# ensure content is hidden
	content.visible = false


func _on_ShowFold_pressed() -> void:
	content.visible = !content.visible
