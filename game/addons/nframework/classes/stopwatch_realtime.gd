class_name StopwatchRT
extends Node

# this is realtime
# it's unaffected by engine slowdowns and pause
# this doesn't use _process so you don't need to add as child


var start_time: int
var last_pause_time: int

var time_paused: int

var paused: bool = false setget pause


func _init() -> void:
	pause_mode = Node.PAUSE_MODE_STOP


func start() -> void:
	start_time = OS.get_ticks_msec()
	time_paused = 0


func get_time() -> float:
	return (OS.get_ticks_msec() - start_time - time_paused) / 1000.0


func pause(value) -> void:
	if value == paused:
		return
	else:
		paused = value

	if paused == true:
		# set mark
		last_pause_time = OS.get_ticks_msec()
	else:
		# add length of pause
		time_paused += OS.get_ticks_msec() - last_pause_time
