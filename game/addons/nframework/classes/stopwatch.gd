class_name Stopwatch
extends Node

# this is gametime
# it's affected by engine slowdowns and pause

# WARNING: remember you have to add this as a child for _process to work

var time: float = -1
var paused: bool = true setget pause


func _init() -> void:
	pause_mode = Node.PAUSE_MODE_STOP
	set_process(false)


func _process(delta: float) -> void:
	time += delta


func start() -> void:
	time = 0.0
	self.paused = false


func stop() -> void:
#	time = -1
	set_process(false)


func pause(value) -> void:
	paused = value
	set_process(!paused)
