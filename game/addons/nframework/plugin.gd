tool
extends EditorPlugin

const BASE_PATH: String = "res://addons/nframework/"
const AUTOLOADS_PATH: String = BASE_PATH + "autoloads/"
const NULL_AUTOLOAD: String = "Null.gd"

# order matters!
# the autoloads will be loaded and appear in the scenetree in this order
# you can disable autoloads by adding them to disabled_autoloads
# almost all autoloads require the core modules, so don't disable those
# you mostly just want to disable the overlays
const autoloads := {
	## core modules
	"Config": "config.gd",
	"Log": "logger.gd",

	# runtime settings
	"SettingsAudio": "settings/settings_audio.gd",
	"SettingsControls": "settings/settings_controls.gd",
	"SettingsVideo": "settings/settings_video.gd",
	"Settings": "settings/settings.gd",

	# overlays
	"TransitionOverlay": "overlay/transition_overlay.tscn",
	"FPSOverlay": "overlay/fps_overlay.tscn",
	"DebugDraw": "overlay/debug_draw/debug_draw.tscn",
	"PauseScreen": "overlay/pause_screen.tscn",

	# systems
	# framework depends on transition overlay and pause system
	"F": "framework.gd",
	"SoundManager": "sound_system/sound_manager.gd",
#	"Music": "sound_system/music.gd",
}

const disabled_autoloads := [
#	"Settings",
#	"SettingsAudio",
#	"SettingsControls",
#	"SettingsVideo",
#	"Pause",
#	"Transition",
#	"FPS"
#	"F",
	]


var dock_file_editor
var dock_config
var dock_resolution


func _enter_tree():
	# Initialization of the plugin goes here.

	for key in autoloads.keys():
		if key in disabled_autoloads:
			add_autoload_singleton(key, AUTOLOADS_PATH + NULL_AUTOLOAD)
		else:
			add_autoload_singleton(key, AUTOLOADS_PATH + autoloads[key])
	add_autoload_singleton("G", "res://global.tscn")

	# Load the dock. Load from file here so it's reloaded on every _enter_tree ?
	dock_resolution = preload("res://addons/nframework/dock/resolution.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_resolution)

	dock_file_editor = preload("res://addons/nframework/dock/file_editor.tscn").instance()
	add_control_to_dock(DOCK_SLOT_LEFT_BR, dock_file_editor)

	dock_config = preload("res://addons/nframework/dock/config.tscn").instance()
	add_control_to_bottom_panel(dock_config, "Config")

	# we cannot use Config here, as the plugin will fail to load the first time around
	# because Config hasn't been set as autoload yet
#	Config.editor_interface = get_editor_interface()
#	get_tree().root.get_node("Config").editor_interface = get_editor_interface()

#	add_custom_type("MultiTrack", "Node",
#		load("res://test/adaptive_music/classes/multi_track.gd"),
#		load("res://addons/nframework/assets/icons/icon_node.svg"))
#
#	add_custom_type("ShuffleMulti", "MultiTrack",
#		load("res://test/adaptive_music/classes/shuffle.gd"),
#		load("res://addons/nframework/assets/icons/icon_node.svg"))
#
#	add_custom_type("RandomMulti", "MultiTrack",
#		load("res://test/adaptive_music/classes/random.gd"),
#		load("res://addons/nframework/assets/icons/icon_node.svg"))

func _exit_tree():
	# Clean-up of the plugin goes here.

	remove_autoload_singleton("G")
	for key in autoloads.keys():
		remove_autoload_singleton(key)

	# Remove the dock.
	remove_control_from_bottom_panel(dock_config)
	dock_config.queue_free()

	remove_control_from_docks(dock_resolution)
	dock_resolution.queue_free()
	remove_control_from_docks(dock_file_editor)
	dock_file_editor.queue_free()

#	remove_custom_type("ShuffleMulti")
#	remove_custom_type("RandomMulti")
#	remove_custom_type("MultiTrack")

