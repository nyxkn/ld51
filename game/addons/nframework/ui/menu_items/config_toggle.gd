tool
extends CheckButton


export(String) var tracking_property = ""

# you can set tracking property in the editor as an export
# or through the init method here through code
#func setup(tracking_property: String) -> Node:
#	self.tracking_property = tracking_property
#	return self


func _ready() -> void:
	assert(tracking_property in Config, "tracking_property = \"" + str(tracking_property) + "\". " +
			"this does not exist in Config")
	text = tracking_property
	pressed = Config.get(tracking_property)


func _on_ConfigToggle_toggled(button_pressed: bool) -> void:
	Config.set(tracking_property, button_pressed)
	Config.save_cfg()

