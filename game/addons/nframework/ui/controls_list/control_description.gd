extends HBoxContainer


const textures_path = "res://assets/temp/controls/"

#onready var label_keyname = $MarginContainer/KeyName


func init(control, description):
	var texture = load(textures_path + control.as_text() + ".tres")
	var label_keyname = $MarginContainer/KeyName

	if not texture or Config.ludum_dare:
		$KeyTexture.hide()
		label_keyname.text = control.as_text()
		label_keyname.show()
	else:
		$KeyTexture.show()
		$KeyTexture.texture = texture
		label_keyname.hide()

	$Description.text = description

	return self
