extends Control


const Score = preload("score.tscn")


# or singleton
var scores


func _ready() -> void:
	for score in scores:
		var score_hbox = Score.instance()
		score_hbox.get_node("PlayerName").text = score.player_name
		score_hbox.get_node("Score").text = str(score.score)
		$Scores.add_child(score_hbox)
