extends Control


onready var button_newgame: Button = $Control/MenuOptions/NewGame
onready var button_settings: Button = $Control/MenuOptions/Settings
onready var button_exit: Button = $Control/MenuOptions/Exit
onready var button_help: Button = $Control/MenuOptions/Help


func _ready():
#	theme = load(Config.theme)
#	$Control/Title.text = ProjectSettings.get_setting("application/config/name")

	if Config.html5:
		button_exit.visible = false

	button_newgame.connect("pressed", self, "_on_NewGame_pressed")
	button_settings.connect("pressed", self, "_on_Settings_pressed")
	button_exit.connect("pressed", self, "_on_Exit_pressed")
	button_help.connect("pressed", self, "_on_Help_pressed")

	if Config.menu_keyboard_enabled:
		setup_focus()

#	if Config.disable_settings:
#		button_settings.visible = false

	if not G.get_node("Intermission").playing:
		G.get_node("Intermission").play()


func setup_focus():
	Utils.setup_focus_grabs_on_mouse_entered($Control/MenuOptions)

	button_newgame.grab_focus()


func _on_NewGame_pressed():
	G.fade_out_intermission()

	# example for ensuring loading has finished before starting
	var loader = get_tree().root.get_node_or_null("PreLoader")
	if not loader or loader.finished:
		F.change_scene(Config.scenes.new_game, true, 1)
	else:
		Log.e("preloader resources are still being loaded. we shouldn't let this happen", name)


func _on_Settings_pressed():
	F.change_scene(Config.scenes.settings_menu)


func _on_Help_pressed():
	F.change_scene(Config.scenes.help)


#func _on_Credits_pressed():
#    pass # Replace with function body.


func _on_Exit_pressed():
	get_tree().quit()


