class_name Utils

## Generic utility functions
## Split into appropriate files if this becomes too large


## simple benchmark function. call this for 10-20 times to simulate a couple seconds lock
static func expensive_function() -> void:
	var lst = []
	for i in 999999:
		lst.append(sqrt(i))


# returns the time in seconds it takes to run function "f", "count" times
# 10^6 is a good number where quick single line operations take about 1s
# 10^7 takes about 10s
static func benchmark_function(f: FuncRef, argv: Array = [], count: int = pow(10, 6)) -> void:
	var start_time: float = OS.get_ticks_msec()
	for i in count:
		f.call_funcv(argv)
	var end_time: float = OS.get_ticks_msec()
	var total_time = end_time - start_time
	total_time = total_time / 1000.0
	Log.d(["function took", total_time, "s to run", count, "times"],
		"benchmark_function")



static func set_margins(margin_container, margin_value: int, margin_rightleft: int = -1) -> void:
	var mc: MarginContainer = margin_container

	if margin_rightleft >= 0:
		mc.add_constant_override("margin_right", margin_rightleft)
		mc.add_constant_override("margin_left", margin_rightleft)
	else:
		mc.add_constant_override("margin_right", margin_value)
		mc.add_constant_override("margin_left", margin_value)

	mc.add_constant_override("margin_top", margin_value)
	mc.add_constant_override("margin_bottom", margin_value)


# recursively get all leaf children of a node
static func get_all_leaf_children(node: Node) -> Array:
	var children := []

	for child in node.get_children():
		if child.get_child_count() > 0:
			children.append_array(get_all_leaf_children(child))
		else:
			children.append(child)

	return children


# recursively get all children of a node
# if you need nodes only of a specific type, just get all of them and reiterate to compare
# it would be nice to add a comparison here but i don't think you can pass a type in here
static func get_all_children(node: Node) -> Array:
	var children := []

	for child in node.get_children():
		children.append(child)

		if child.get_child_count() > 0:
			children.append_array(get_all_children(child))

	return children


# probably not very useful at all and maybe even troublesome
#static func set_owner_on_all_children(node: Node, owner: Node = null) -> void:
#	var children = get_all_children(node)
#	for child in children:
#		child.owner = owner if owner else node


## store all children of a node into a given array
## node: node to recurse into
## children: array to store children in
static func store_children_recursive(node: Node, children: Array = [], recurse_level: int = 0) -> void:
#	Log.d("    ".repeat(recurse_level) + "[" + node.name + "]")

	for n in node.get_children():
		if n.get_child_count() > 0:
			children.append(n)
			store_children_recursive(n, children, recurse_level + 1)
		else:
			children.append(n)
#			Log.d("    ".repeat(recurse_level + 1) + "- " + n.name)


static func grab_focus_on_mouse_entered(control: Control) -> void:
	control.connect("mouse_entered", control, "grab_focus")


static func setup_focus_grabs_on_mouse_entered(control: Control) -> void:
	var children: Array = []
	store_children_recursive(control, children)

	for c in children:
		if c is Button or c is Slider:
			grab_focus_on_mouse_entered(c)


## look into a dictionary to find the desired value. return its key.
static func dict_get_key_of_value(dict: Dictionary, value_search):
	for k in dict.keys():
		if k == value_search:
			return k
	return null


# dictionaries are ordered, so the order of parameters here matters
# values of dictionary b will overwrite same keys in dictionary a
static func merge_dict(a: Dictionary, b: Dictionary) -> Dictionary:
	var merged = a.duplicate()
	for key in b:
		merged[key] = b[key]

	return merged


## logging of input events in a sane non-overwhelming way
static func log_event(event: InputEvent, function, name):
	if event is InputEventMouseButton and event.is_pressed():
		Log.d([event.get_class(), function], name)
		# Log.d([event.as_text(), function], name)
		if name == "InputThings": print("--------")
	# elif event is InputEventMouseMotion:
	# 	Log.d([event.get_class(), function], name)
	elif not event is InputEventMouseMotion and event.is_pressed():
		Log.d([event.as_text(), function], name)
		if name == "InputThings": print("--------")


## reparent node onto new parent
## optionally update the ownership to the new parent as well
## this isn't generally needed
static func reparent(child: Node, new_parent: Node, reset_owner: bool = false) -> void:
	var old_parent = child.get_parent()
	old_parent.remove_child(child)
	new_parent.add_child(child)
	if reset_owner: child.owner = new_parent


## return a random color
static func random_color() -> Color:
	return Color(rand_range(0,1), rand_range(0,1), rand_range(0,1))



static func print_children(node: Node) -> void:
	for child in node.get_children():
		Log.d(child.name)


#static func panic(message: String) -> void:
#	# Will automatically print an error message in the console as well.
#	OS.alert(message)
#	# Non-zero exit code to indicate failure.
#	get_tree().quit(1)


################
# DEPRECATED
################

## pretty print array
static func array_to_printable_string(array) -> String:
	var string := ""
	for e in array:
		string += str(e, ", ")
	return string.substr(0, string.length() - 2)


# i have no idea what use we might have had for this
# this would ideally be a variadic function
static func debug_print(varargs_array) -> void:
	print(array_to_printable_string(varargs_array))
