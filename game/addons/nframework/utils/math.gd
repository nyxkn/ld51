class_name Math

## A collection of useful math functions

## compute the greatest common divisor
## with euclidean algorithm
## https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations
static func gcd(a: int, b: int) -> int:
	if b == 0:
		return a
	else:
		return gcd(b, a % b)


## solve quadratic equations with quadratic formula
## returns an array with the positive and negative answers
static func quadratic(a: float, b: float, c: float) -> Array:
	# x = (-b +- sqrt(b^2 - 4ac)) / 2a

	var square_root = sqrt(b*b - 4.0*a*c)
	var a2 = 2.0*a
	var plus_x = (-b + square_root) / a2
	var minus_x = (-b - square_root) / a2

	return [plus_x, minus_x]


static func avg(a: Array) -> float:
	if a.size() == 0:
		return 0.0

	var sum: float = 0.0
	for n in a:
		sum += n
	return sum / a.size()
