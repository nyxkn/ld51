tool
extends HBoxContainer


var scene_name: String = ""


func _ready() -> void:
	$Set.text = scene_name
	update()


func _on_Set_pressed() -> void:
	Config.scenes[scene_name] = Config.editor_interface.get_current_path()
	Config.save_cfg()
	update()


func update() -> void:
	$SceneFile.text = Config.scenes[scene_name]


func _on_Reset_pressed() -> void:
	Config.scenes[scene_name] = Config.default_scenes[scene_name]
	Config.save_cfg()
	update()
