tool
extends Control


var access_modes := {
	'resources': FileDialog.ACCESS_RESOURCES,
	'userdata': FileDialog.ACCESS_USERDATA,
	'filesystem': FileDialog.ACCESS_FILESYSTEM,
	}

var current_file: String = ""
var encryption_unique_id: bool = false
#var current_encryption_pass: String = ""

onready var ob_access: OptionButton = $VBoxContainer/Controls/Access
onready var file_dialog: FileDialog = $FileDialog
onready var save_dialog: FileDialog = $SaveDialog
onready var text_edit: TextEdit = $VBoxContainer/TextEdit
onready var unique_id: String = OS.get_unique_id()


func _ready() -> void:
	ob_access.clear()
	for a in access_modes:
		ob_access.add_item(a)


func get_encryption_pass():
	var password := ""
	if encryption_unique_id:
		password = unique_id
	else:
		password = ""

	return password


func update_ui() -> void:
	$VBoxContainer/FileName/Path.text = current_file


func _on_Load_pressed() -> void:
	file_dialog.popup_centered_ratio(0.5)


func _on_Save_pressed() -> void:
#	save_dialog.popup_centered_ratio(0.5)
	if current_file:
		FileUtils.write_to_file(text_edit.text, current_file, get_encryption_pass())
	else:
		Log.i(["no file selected for writing"], name)


func _on_Reset_pressed() -> void:
	current_file = ""
	$VBoxContainer/Encryption/UniqueID.pressed = false
	text_edit.text = ""
	update_ui()


func _on_Access_item_selected(index: int) -> void:
	var value = ob_access.get_item_text(index)
	file_dialog.access = access_modes[value]
	match value:
		'userdata':
			file_dialog.current_dir = "user://"


func _on_FileDialog_file_selected(file_path: String) -> void:
	current_file = file_path
	print(current_file)
	var content = FileUtils.read_file(current_file, get_encryption_pass())
	text_edit.text = content
	update_ui()


func _on_UniqueID_toggled(button_pressed: bool) -> void:
	encryption_unique_id = button_pressed


