extends Node

signal loading_finished

#var ParticlesWorkspace := preload("res://game/particles/particles_workspace.tscn")

#const MainMenu := preload("res://addons/nframework/scenes/main_menu.tscn")

var use_threads = false


# this is the true entry point of the whole application
func _ready():
	# for peace of mind, actually start doing all your initialization on the next frame
	# so that everything is properly initialized by then
	# otherwise root doesn't seem to be fully initialized
	call_deferred("main")


func config_setup():
	Config.disable_settings = true
	Config.straight_to_game = false

	Config.scenes = {

		loading = "res://addons/nframework/scenes/loading.tscn",
		main_menu = "res://addons/nframework/scenes/main_menu.tscn",
		settings_menu = "res://addons/nframework/scenes/settings_menu.tscn",

		# this should point to your game actual entry point
#		new_game = "res://game.tscn",
		battle = "res://scenes/battle.tscn",
		intermission = "res://scenes/intermission.tscn",
		intro = "res://scenes/intro.tscn",
		ending = "res://scenes/ending.tscn",
		new_game = "res://scenes/new_game.tscn",
		help = "res://scenes/help.tscn",
	}


	Log.i("config values set", name)


# our main function where we initialize everything and change to a different scene
func main() -> void:
	config_setup()

	# at this point, godot has fully initialized and all autoloads have been loaded
	Log.i(["startup took", F.ticks(), "s"], name)

	# debug build or running from editor
	if Config.debug:
		pass

	# exported release build
	if not Config.debug:
		# custom config settings
		Config.log_hide_level = {
			Log.Level.DEBUG: true,
		}
		Config.straight_to_game = false

	if Config.silentwolf:
		# load at runtime so that it won't complain if silentwolf is missing
		var SilentWolfInit := load("res://addons/nframework/autoloads/scores/silentwolf_init.gd")
		get_tree().root.add_child(SilentWolfInit.new())

	if Config.html5:
		use_threads = false

	var loader
	if use_threads:
		loader = PreLoader.new()
		# only works if root has fully loaded. if you call this from _ready it won't work
		get_tree().root.add_child(loader)
		# just playing it safe after adding the child
		yield(get_tree(), "idle_frame")

	unthreaded_preload()

	if Config.straight_to_game:
		# wait for loading to finish
		if use_threads:
			yield(loader, "loading_finished")
		F.change_scene(Config.scenes.new_game, false)
	else:
		# instead of waiting here you can also wait in the menu or intro screen
		# waiting in the main menu works well if you have to spend at least a few seconds menuing
		# loading screen with progress spinner is a safe default
		# or if you have an intro screen you can load silently while the intro is playing

		# add loading screen if the load is long
		# otherwise if it would show too briefly then it's best not to show anything at all
#		add_child(load(Config.scenes.loading).instance())

		if use_threads:
			yield(loader, "loading_finished")
#		F.change_scene(Config.scenes.main_menu, false)
		F.change_scene(Config.scenes.intro, false)


func unthreaded_preload():
	var start_time = F.ticks()
	Log.d("unthreaded loading started", name)

#	SoundManager.load_samples_folder("res://assets/sfx/")
	SoundManager.load_samples_folder("res://assets/jfxr/")


	G.totaltime_stopwatch = StopwatchRT.new()
	G.totaltime_stopwatch.start()

	SettingsAudio.set_volume("SFX", 0.4)
	SettingsAudio.set_volume("Master", 0.4)


	Log.d("unthreaded loading finished", name)
	Log.i(["unthreaded loading took", F.ticks() - start_time, "s"], name)


class PreLoader:
	extends Node

	signal loading_finished

	var finished := false
	var thread := Thread.new()

	func _init() -> void:
		name = "PreLoader"
		thread.start(self, "start_loading", "")


	# even if loading is very brief, this is more flexible than preloading
	# here we only load data that isn't immediately needed
	# everything else like settings and config should be loaded earlier in the main thread
	func start_loading(userdata) -> void:
		var start_time = F.ticks()
		Log.d("loading started", name)

		# load particles
	#	var pw = ParticlesWorkspace.instance()
	#	pw.visible = false
	#	# to get _ready to run we have to add_child. then we remove immediately
	#	add_child(pw)
	#	pw.queue_free()

		# load sounds
		SoundManager.load_samples_folder("res://assets/sfx/")
		SoundManager.load_samples_folder("res://assets/jfxr/")

		# load test images. 10 files for about 200mb total
#		for i in 10:
##			$TextureRect.texture = load("res://assets/temp/chan/" + str(i) + ".png")
#			load("res://assets/temp/chan/" + str(i) + ".png")
#			print("loaded asset " + str(i))

		Log.d("loading finished", name)
		Log.i(["loading resources took", F.ticks() - start_time, "s"], name)
		finished = true
		call_deferred("emit_signal", "loading_finished")
		queue_free()


	func _exit_tree() -> void:
		pass
		# probably no need for this. we should be able to exit the application while it's loading
		# and there doesn't seem to be the problem of this being removed before it finishes
		# because it's run as an autoload and we only free it when loading is finished
		thread.wait_to_finish()
		# but debugger complains if we don't add this line. what to do?



