extends Node

onready var text_box := $TextBox

func _ready() -> void:
	text_box.queue_text("1. The quick brown fox jumps over the lazy dog.")
	text_box.queue_text("2. The quick brown fox jumps over the lazy dog.")
