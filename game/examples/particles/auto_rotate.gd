extends Node2D

export var speed := 1.0
var time := 0.0

func _process(delta: float) -> void:
	time = wrapf(time + delta * speed, 0, TAU)
	rotation = time
