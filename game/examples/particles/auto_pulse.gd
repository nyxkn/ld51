extends Node2D

export var power := 0.05
export var speed := 20.0
var time := 0.0
onready var original_scale := scale

func _process(delta: float) -> void:
	time = wrapf(time + delta * speed, 0, TAU)
	scale = original_scale + (Vector2.ONE * sin(time) * power)
