extends KinematicBody2D

## movement look at mouse

# pixels per second
export (int) var speed = 100

var velocity: Vector2 = Vector2.ZERO


func read_input() -> Vector2:
	var input := Vector2.ZERO

	if Input.is_action_pressed("down"):
		input.y += -1
	if Input.is_action_pressed("up"):
		input.y += 1

	return input


func _physics_process(_delta):
	# look_at aligns the +x axis to the target
	look_at(get_global_mouse_position())
	var input := read_input()
	velocity = transform.x * input.y * speed
	velocity = move_and_slide(velocity)
