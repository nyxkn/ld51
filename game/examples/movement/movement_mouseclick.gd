extends KinematicBody2D

## movement mouse click

# pixels per second
export (int) var speed = 100

var velocity: Vector2 = Vector2.ZERO
var last_click_location: Vector2 = Vector2.INF


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.is_pressed():
			last_click_location = get_global_mouse_position()
			look_at(last_click_location)


func _physics_process(_delta):
	if last_click_location != Vector2.INF:
		var difference = last_click_location - position
		if difference.length() < 1:
			last_click_location = Vector2.INF
		else:
			velocity = difference.normalized() * speed
			velocity = move_and_slide(velocity)
