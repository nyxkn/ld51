extends KinematicBody2D

# it's helpful to think of speed units in terms of screen size per second
# at the very least it's a portable way of defining sane ballpark values
# unfortunately it doesn't seem we can use variables in the definition of export variables
# so we have to keep these regular non-exported variables

# px/s/s
var gravity := Vector2(0, F.screen_size.y * 5)

# this is how fast we accelerate to speed
var acceleration_magnitude := F.screen_size.x * 5
# this is how fast we decelerate to a stop
# if you think of a human moving, this value can be tweaked at will by the person
# e.g. one could stop very quickly by using more force, or slowly with less force
var deceleration_magnitude := F.screen_size.x * 3

# controls whether we use lateral acceleration or if we move instantly to speed
var lateral_acceleration := true

# lateral movement speed in px/s
var speed := F.screen_size.x / 2
# initial jump velocity in px/s
var jump_speed := F.screen_size.y * 1.5
var max_jumps := 2


# ================
# private

# storing current velocity
var velocity: Vector2 = Vector2.ZERO
var jumps_available := max_jumps


func read_input() -> Vector2:
	# clear input vector so we can read the new inputs
	var input := Vector2.ZERO

	# we sum left and right together, and up and down together
	# this deals with continuous input (you can keep pressing the key for the effect to repeat)
	# if you need to take care of actions that happen only once, e.g. jumping
	# then you need to use is_action_just_pressed
	if Input.is_action_pressed("left"):
		input.x += -1
	if Input.is_action_pressed("right"):
		input.x += 1

	return input


func _physics_process(delta: float) -> void:
	var input := read_input()

	if lateral_acceleration:
		if input.x:
			var acceleration_vector = input.x * acceleration_magnitude
			velocity.x = velocity.x + acceleration_vector * delta
			# accelerating to speed and not more
			velocity.x = sign(velocity.x) * min(abs(velocity.x), speed)
			print(velocity.x)
		else:
			var deceleration_vector = -sign(velocity.x) * deceleration_magnitude
			velocity.x = velocity.x + deceleration_vector * delta
			if abs(velocity.x) < 1:
				velocity.x = 0
	else:
		# no acceleration. very simply set velocity to speed on movement
		velocity.x = input.x * speed

	# to figure out the new velocity, we use v1 = v0 + a*t
	# move_and_slide multiplies by delta once more but that's to convert px/frame into px/second
	velocity.y = velocity.y + gravity.y * delta
	velocity = move_and_slide(velocity, Vector2.UP)
#	var collision = move_and_collide(velocity)

	if is_on_floor():
		jumps_available = max_jumps

	# using is_action_just_pressed to deal with jumping
	if Input.is_action_just_pressed("up"):
		jump()


func jump() -> void:
	# set velocity to jump_speed
	# note that we're not adding jump_speed to velocity, but replacing instead
	# this ensures jump behaves the same way regardless of current gravity effect
	# e.g. the double jump will works exactly the same even if while in the air
	# the velocity accumulated from gravity is going to be greater
	if jumps_available > 0:
		jumps_available -= 1
		velocity.y = -jump_speed
		print("jump")
