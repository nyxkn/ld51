extends KinematicBody2D

## movement asteroids

# pixels per second
export(int) var speed: int = 100
# radians per second
export(float) var angular_speed: float = 0.1

var velocity: Vector2 = Vector2.ZERO


func read_input() -> Vector2:
	var input := Vector2.ZERO

	if Input.is_action_pressed("left"):
		input.x += -1
	if Input.is_action_pressed("right"):
		input.x += 1
	if Input.is_action_pressed("down"):
		input.y += 1
	if Input.is_action_pressed("up"):
		input.y += -1

	return input


func _physics_process(_delta):
	var input := read_input()
	# rotate this node
	rotation += input.x * angular_speed
	# apply velocity onto our rotated transform axis
	velocity = transform.y * input.y * speed
	velocity = move_and_slide(velocity)
