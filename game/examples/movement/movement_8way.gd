extends KinematicBody2D

## movement 8-way

# pixels per second
export (int) var speed = 100

var velocity: Vector2 = Vector2.ZERO


# alternative way of getting input
# this is how gdquest does it
# this also takes joystick speed into account
func read_input_alt():
	# this
	var input := Vector2(
		Input.get_action_strength("right") - Input.get_action_strength("left"),
		Input.get_action_strength("down") - Input.get_action_strength("up")
	)
	return input


func read_input() -> Vector2:
	# clear input vector so we can read the new inputs
	var input := Vector2.ZERO

	# we sum left and right together, and up and down together
	if Input.is_action_pressed("left"):
		input.x += -1
	if Input.is_action_pressed("right"):
		input.x += 1
	if Input.is_action_pressed("down"):
		input.y += 1
	if Input.is_action_pressed("up"):
		input.y += -1

	return input


# we do our movement code in _physics_process
# because our character needs to collide with other physics bodies
func _physics_process(_delta):
	var input := read_input()
	# normalize so diagonal directions move at the same speed as cardinal directions
	velocity = input.normalized() * speed
	velocity = move_and_slide(velocity)
