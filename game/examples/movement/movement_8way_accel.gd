extends KinematicBody2D

## movement 8-way

# pixels per second
export (int) var max_speed = 100
# pixels per second per second
export (int) var accel_magnitude = 100
export (int) var friction = 200

var speed: float = 0.0

var velocity: Vector2 = Vector2.ZERO
var accel_vector: Vector2 = Vector2.ZERO

func read_input() -> Vector2:
	# clear input vector so we can read the new inputs
	var input := Vector2.ZERO

	# we sum left and right together, and up and down together
	if Input.is_action_pressed("left"):
		input.x += -1
	if Input.is_action_pressed("right"):
		input.x += 1
	if Input.is_action_pressed("down"):
		input.y += 1
	if Input.is_action_pressed("up"):
		input.y += -1

	return input


func _ready() -> void:
	$Debug.add_funcref(funcref(self, "debug_data"), "velocity")


func debug_data():
	return [accel_vector, velocity]


# a = dv / dt
# dv = a * dt
# vf = vi + (a * dt)

func _physics_process(delta):
	var input := read_input()

	if input != Vector2.ZERO: # we have input
		var direction = input.normalized()
		accel_vector = direction * accel_magnitude
	else: # we don't have input. stopping.
		if velocity.length() < 5:
			# stop condition when close enough, because of float math imprecision
			accel_vector = Vector2.ZERO
			velocity = Vector2.ZERO
		else:
			# using our last velocity as a normalized direction vector
			# applying acceleration in the opposite direction of that
			accel_vector = -velocity.normalized() * friction

	var vf: Vector2 = velocity + accel_vector * delta
	vf = vf.clamped(max_speed)
	velocity = move_and_slide(vf)

	$VectorDraw.draw([accel_vector, velocity])
