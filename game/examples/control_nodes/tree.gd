extends Tree

var tree: Tree = self

func _ready() -> void:
	var root = tree.create_item()
#	tree.set_hide_root(true)

	tree.set_column_title(0, "c0")
	tree.set_column_title(1, "c1")
	tree.set_column_title(2, "c2")

	var child1 = tree.create_item(root)
	child1.set_text(0, "child1-c0")
	child1.set_text(1, "child1-c1")
	child1.set_text(2, "child1-c2")

	var child2 = tree.create_item(root)
	child2.set_text(0, "child2")


	var subchild1 = tree.create_item(child1)
	subchild1.set_text(0, "subchild1-c0")
	subchild1.set_text(1, "subchild1-c1")
	subchild1.set_text(2, "subchild1-c2")

	var subchild2 = tree.create_item(child2)
	subchild2.set_text(0, "subchild2")
