extends GraphNode


func _ready() -> void:
	var idx := 0
	var type := 0
	set_slot(idx, true, 0, Color.white, true, type, Color.white)


func _on_GraphNode_resize_request(new_minsize: Vector2) -> void:
	rect_size = new_minsize


func _on_GraphNode_close_request() -> void:
	queue_free()
