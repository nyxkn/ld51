extends Control

# emi
# p1: https://www.youtube.com/watch?v=ZD9X3uvyWmg
# p2: https://www.youtube.com/watch?v=AX5pN3mwfBc


onready var graph = $GraphEdit


func _ready() -> void:
	graph.connect_node("GraphNode1", 0, "GraphNode2", 0)


func _on_GraphEdit_connection_request(from: String, from_slot: int, to: String, to_slot: int) -> void:
	graph.connect_node(from, from_slot, to, to_slot)


func _on_GraphEdit_disconnection_request(from: String, from_slot: int, to: String, to_slot: int) -> void:
	graph.disconnect_node(from, from_slot, to, to_slot)


func _on_AddNode_pressed() -> void:
	pass # Replace with function body.
