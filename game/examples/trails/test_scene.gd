extends Node2D


const SmokeTrail := preload("smoke_trail.tscn")
const Bullet := preload("bullet.tscn")

var last_shoot_time := 0.0
# 1 over N, where N is the rate of fire in shots per seconds
var shoot_delay := 1.0 / 5


func can_shoot() -> bool:
	return true if F.time() > last_shoot_time + shoot_delay else false


func _process(delta: float) -> void:
	pass
#	$SmokeTrailTest.add_point(get_global_mouse_position())
	$FireDeco.global_position = get_global_mouse_position()

	if Input.is_action_pressed("ui_click") and can_shoot():
		var bullet = Bullet.instance()
		add_child(bullet)
		bullet.target = $Target
		bullet.direction = Vector2.RIGHT.rotated(rand_range(-0.1, 0.1))
		bullet.position = get_global_mouse_position()
		last_shoot_time = F.time()
