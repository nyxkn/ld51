extends Line2D


export var limited_lifetime := false
export var lifetime := [ 1.0, 2.0 ]
export var wildness := 3.0
export var age_wildness := 1.0
#export var min_spawn_distance := 5.0
export var gravity := Vector2.UP
export var wind_direction := Vector2(15, 5)
export var gradient_color: Gradient = Gradient.new()
export var max_points := 20
export var tick_time := 0.05
export var wind_strength := 1.0
export var turbulence := 1.0

var tick := 0.0
var wild_speed := 0.02
var point_age := [0.0]
var noise := OpenSimplexNoise.new()
var noise_seek := 0.0
var wind_sway := 0.0

func _ready() -> void:
	noise.octaves = 2
	set_as_toplevel(true)
	clear_points()
	gradient = gradient_color
	if limited_lifetime:
		fade()


func _process(delta: float) -> void:
	# speed of seeking through the noise
	# will be affected by the noise settings as well
	noise_seek += delta * 100
	if tick > tick_time:
		tick = 0
		for p in get_point_count():
			if p == get_point_count() - 1:
				continue

			point_age[p] += delta
			# turbulence is how fast we go through the noise
			var noise_x = points[p].x + noise_seek * turbulence
			var noise_y = points[p].y + noise_seek * turbulence
			# lerping to smoothen the change. look slightly different than using the noise directly
			# anyway wrapping this in lerp is good. just set t to 1.0 for the unlerped version
			wind_sway = lerp(wind_sway, noise.get_noise_2d(noise_x, noise_y) * point_age[p] * 0.5 * wind_strength, 0.2)

			var rand_vector = Vector2(rand_range(-wild_speed, wild_speed), rand_range(-wild_speed, wild_speed))
			points[p] += gravity + (rand_vector * wildness * (point_age[p] * age_wildness)) + (wind_direction * wind_sway)

	else:
		tick += delta


func fade():
	var decay_time = rand_range(lifetime[0], lifetime[1])
	$Decay.interpolate_property(self, "modulate:a", 1.0, 0.0, decay_time, Tween.TRANS_CIRC, Tween.EASE_OUT)
	$Decay.start()
	print("fade")
#	queue_free()


# override line2d function
func add_point(point_pos:Vector2, at_pos := -1):
#	if get_point_count() > 0 and point_pos.distance_to(points[-1]) < min_spawn_distance:
#		return

	if get_point_count() > max_points:
		remove_point(0)
		point_age.pop_front()

	point_age.append(0.0)
	.add_point(point_pos, at_pos)


func _on_Decay_tween_all_completed() -> void:
	queue_free()
