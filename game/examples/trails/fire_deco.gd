extends Node2D


onready var smoke_trail_static = $SmokeTrailStatic
onready var fire = $Spotlight2

var time := 0.0


func _ready() -> void:
	pass


func _process(delta: float) -> void:
	time += delta
	if time > 0.05:
		smoke_trail_static.add_point(global_position)
		# multiply by one? why
		fire.scale = Vector2.ONE * rand_range(0.2, 0.1)
		time = 0.0
