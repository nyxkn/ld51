extends Area2D

var direction := Vector2.RIGHT
var speed := 10.0
var dead := false
var target: Node2D = null

onready var smoke_trail := $SmokeTrail


func _ready() -> void:
	$Explosion.scale = Vector2.ZERO


func _process(delta: float) -> void:
	if !dead:
		position += direction * speed
		smoke_trail.add_point(global_position)

		direction = direction.rotated(rand_range(-0.2, 0.2))
		direction = lerp(direction, global_position.direction_to(target.global_position), 0.05)


func _on_Bullet_body_entered(body: Node) -> void:
	print("body")
	if !dead:
		dead = true
		speed = 0.0
		smoke_trail.fade()
		$AnimationPlayer.play("explosion")


func _on_Timer_timeout() -> void:
	_on_Bullet_body_entered(null)
