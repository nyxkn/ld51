extends Node

var level_id := 0

func _ready():
	$LevelManager.init_levels("res://examples/level_manager/test_levels/")
	$LevelManager.change_level(level_id, false)

func _input(event):
	if event.is_action_pressed("ui_accept"):
		level_id += 1
		if level_id > 1: level_id = 0
		$LevelManager.change_level(level_id, true, true)
