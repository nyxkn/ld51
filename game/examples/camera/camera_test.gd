extends Node


func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_1:
			$Camera.add_trauma(0.3)
		if event.scancode == KEY_2:
			$Camera.add_trauma(0.5)
		if event.scancode == KEY_3:
			$Camera.add_trauma(1.0)
