tool
extends Node2D

## simply duplicate particle0 as your starting point
## then reload the scene with c-s-r to save
## set one_shot to true when you're using the particles


func _ready() -> void:
	pass
	# XXX take care that if you edit a particle and don't run the build it won't save
	# for this reason we are temporarily loading this scene in main.gd
	# you can also manually reload the scene to save
	if OS.is_debug_build():
		save_all_in(self)


func save_all_in(node: Node) -> void:
	for child in node.get_children():
		if child is Particles2D:
			FileUtils.save_scene("res://game/particles", child)
			Log.d(["saving", child], name)
		elif child.name.begins_with("Particle") and child.name != "Particle0":
			save_all_in(child)
