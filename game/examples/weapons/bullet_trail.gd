extends Line2D


export(bool) var limited_lifetime := false

var lifetime := [ 0.2, 0.5 ]


func _ready() -> void:
	set_as_toplevel(true)
	if limited_lifetime:
		begin_decay()


func begin_decay():
	var decay_time = rand_range(lifetime[0], lifetime[1])
	$Decay.interpolate_property(self, "modulate:a", 1.0, 0.0, decay_time, Tween.TRANS_CIRC, Tween.EASE_OUT)
	$Decay.start()


func _process(delta: float) -> void:
	pass
