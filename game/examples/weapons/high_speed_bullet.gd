extends Node2D


var velocity := Vector2.ZERO
# speed in pixels per second
var speed = 4000

onready var physics_fps = Engine.iterations_per_second


var colliding
var collider

func _ready() -> void:
	# speed in pixels per frame plus an extra pixel. this seems to be enough
	# without extra sometimes the collision doesn't register. probably due to rounding
	# the idea is that we look ahead by the amount that we will move in the next frame
	# this way we are guaranteed to collide in the next frame
	var look_ahead = (speed / physics_fps) + 1
	$RayCast2D.cast_to = Vector2(look_ahead, 0)
	set_as_toplevel(true)


func init(pos, velocity):
	position = pos
	self.velocity = velocity
	rotation = atan2(velocity.y, velocity.x)

	return self



func _process(delta: float) -> void:
	pass


func _physics_process(delta: float) -> void:
	position += velocity * delta * speed

	if colliding:
		collider.hit()
		colliding = false

	if $RayCast2D.is_colliding():
		## about to collide in the next frame
		collider = $RayCast2D.get_collider()
		if not collider.is_queued_for_deletion():
			colliding = true
		print(collider)

