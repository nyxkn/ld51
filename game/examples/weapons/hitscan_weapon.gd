extends Node2D


const Trail := preload("bullet_trail.tscn")
const Impact := preload("impact_effect.tscn")


func shoot():
	# setting emitting = true seems to only work after a delay. restart is the right call
#	$Muzzle.emitting = true
	$Muzzle.restart()
	if $RayCast2D.is_colliding():
		var collider = $RayCast2D.get_collider()
		var collision_point = $RayCast2D.get_collision_point()

		visual_impact(collision_point)
#		static_trail_random(collision_point)
		moving_trail(collision_point)

		# call a hit function if you need
#		if collider.is_in_group("zombie"):
#			collider.hit()
	else:
		var path_v = get_global_mouse_position() - global_position
		var direction = path_v.normalized()
		var target = (global_position + direction * 2000)
		# trail to outside of the screen (ad infinitum)
		moving_trail(target)


func visual_impact(target):
	var impact = Impact.instance()
	add_child(impact)

	impact.set_as_toplevel(true)
	impact.global_position = target
	var path_vector = target - global_position
	impact.rotation = atan2(path_vector.y, path_vector.x)

#	impact.get_node("Particles2D").emitting = true
	impact.get_node("Particles2D").restart()


## draws a trail that moves from the weapon to the target
## this works better with slower rate of fire
func moving_trail(target):
	var difference = target - global_position
	var trail_length = 200
	var trail_start = global_position
	var trail_end = global_position + difference.clamped(trail_length)

	var trail = Trail.instance()
	add_child(trail)
	trail.position = global_position
	trail.look_at(get_global_mouse_position())

	var travel_end = global_position + difference - difference.clamped(trail_length)
	var tween = trail.find_node("Travel")
	tween.interpolate_property(trail, "position", null, travel_end, 0.05)
	tween.start()

	yield(tween, "tween_all_completed")
	trail.lifetime = [ 0.01, 0.02 ]
	trail.begin_decay()


## draws a static trail at a random position between weapon and target
## this looks better with rapid fire and it looks much more varied
func static_trail_random(target):
	var difference = target - global_position
	var distance = difference.length()

	var min_length = 200
	var max_length = 400
	var trail_length = min(rand_range(min_length, max_length), distance)

	var trail_distance = rand_range(trail_length / 2, distance - trail_length / 2)

	var trail_start = global_position + difference.clamped(trail_distance - trail_length / 2)
	var trail_end = global_position + difference.clamped(trail_distance + trail_length / 2)
	var trail = draw_trail(trail_start, trail_end)

	trail.lifetime = [ 0.05, 0.1 ]
	trail.begin_decay()


func draw_trail(from, to) -> Line2D:
	var trail := Trail.instance() as Line2D
	add_child(trail)
	trail.clear_points()
	trail.add_point(from)

	var t := 0.1
	while t <= 0.9:
		trail.add_point(lerp(from, to, t))
		t += 0.1

	trail.add_point(to)

	return trail
