extends Node

# define global variables in here
# ensure this is added to autoloads
# other than for global variables
# this is also useful for static variables for classes (can't have them inside the actual classes)

var gametime_stopwatch: Stopwatch
var totaltime_stopwatch: StopwatchRT

var stage: int = 0
var stage_clear_time := []

var best_stage: int = 0
#var best_time: int
var best_score: int = 0


var battle
var shake
var textbox

const MusicBattle = preload("res://assets/music/battle.ogg")
const MusicFanfare = preload("res://assets/music/fanfare.ogg")
const MusicIntermission = preload("res://assets/music/intermission.ogg")


#func _on_Fanfare_finished() -> void:
#	if battle.phase == battle.Phase.INTERMISSION:
#		yield(F.wait(1.0), "completed"))
#		if battle.phase == battle.Phase.INTERMISSION:
#			$Intermission.play()


func fade_out_intermission():
	var tween = $Tween
	tween.interpolate_property($Intermission, "volume_db", 0, -80, 2.0)
	tween.start()
	yield(tween, "tween_all_completed")
	$Intermission.stop()
	$Intermission.volume_db = 0


func play_fanfare():
	$Fanfare.volume_db = -6
	var tween = $Tween
	tween.interpolate_property($Fanfare, "volume_db", -6, 0, 0.1)
	tween.start()
	$Fanfare.play()

func stop_all():
	$Battle.stop()
	$Fanfare.stop()
	$Intermission.stop()
